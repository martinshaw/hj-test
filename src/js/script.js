var handleTweets, changeTweet;

handleTweets = (tweets) => {
	$("#tweets").html("");
	for (var i = 0; i < tweets.length; i++) {
		$tweet = $(tweets[i]);
		$tweet = $tweet.detach(".interact");
		$tweet = $tweet[0];
		let is_active = (i == 0) ? "active" : "";
		$(".dot").first().addClass(is_active);
		$("#tweets").append($("<li class='tweet "+is_active+"'></li>").html($tweet.innerHTML));
	}
}

changeTweet = (index) => {
	$(".dot").removeClass("active");
	$(".tweet").removeClass("active");
	$(".dot").eq(index).addClass("active");
	$(".tweet").eq(index).addClass("active");
}


$(document).ready(function() {

	var twitterConfig = {
		"profile": {"screenName": 'ukfast'},
		"domId": 'twitter',
		"maxTweets": 4,
		"enableLinks": false, 
		"showUser": false,
		"showTime": false,
		"showImages": false,
		"lang": 'en',
		"showRetweet": false,
		"customCallback": handleTweets,
	};
	twitterFetcher.fetch(twitterConfig);

});