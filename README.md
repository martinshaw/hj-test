**To see the pixel-perfect, responsive and indistinguishable result of this trial task, please see the screenshot below: https://gitlab.com/martinshaw/hj-test#screenshot**

# Single page test project

## Important
**This project will be based primarily on your abilty to create a pixel-perfect reproduction of the designs**.
Fancy bells & whistles, clean code, javascript, innovation etc are all great for bonus points if you get time, but by far the most important part is the precision and execution of the design into the browser.

## Instructions
This is a fairly basic front-end piece to give us a general idea of where you are as a developer. We don't expect you to spend several days creating a fully finished page, but try and spend a few hours on it and see how far you can get. If you're short on time we'd like to see the top half of the page done well, rather than the whole page being rushed. You should use it to demonstrate the following:
- **Version control:** Ideally you will fork from this repo and provide us a link to your new repo containing the finished files.
- **Basic task runner workflow:** There are instructions below for getting going in this very basic gulp project, but you should be comfortable using Gulp to compile SCSS and concatenate JS.
- **Javascript/jQuery:** This design presents a couple of opportunities to use JS. Whether you're creating a slider for the twitter feed or getting creative with visual aspects of the page, we don't expect you to code it all from scratch. Feel free to use external libraries, but keep it tasteful.
- **SCSS:** Make the most of the added functionality SCSS brings to the table.
- **Responsive:** Don't worry about coding for every possible device. All we ask is that it looks ok on screens larger than 1024px and smaller than 500px (Just desktop and mobile).
- **Keen eye for detail:** Your main priority here should be pixel perfect reproduction of the design in the PSD.
- **Clean, readable code**

## Resources
This repo contains the follow resources you'll need to complete the task:
- **Design:** The one page design is available in PSD, PDF and JPG format in the `design` folder.
- **SCSS Framework:** There is a barebones SCSS framework & grid included in the `src/css` folder. Don't feel obliged to use it, you're free to remove all of this and use frameworks or methods you're more comfortable with.
- **Images:** All of the images needed for this page have already been exported and included in the `img` folder.
- **Fonts:** This design uses `Open Sans` and `Font Awesome`, both of which have already been included via their respective CDN's in the `<head>`.

## Installation
1. Clone repo
2. Run `npm install` to install dependencies
3. Run `gulp` to initiate local server and watch for changes
4. CSS and JS are edited in '/src', then compiled with gulp to '/dist'

*You can run `gulp build` to create assets without server*

## Production Build
1. Run `gulp build --production` to disable sourcemaps and add minification

## Screenshot

| Design Brief | Webpage on a large desktop browser | Webpage on a mobile browser |
|-------|-------|-------|
| ![](https://gitlab.com/martinshaw/hj-test/-/raw/master/design/Pentest-Homepage.jpg?inline=false) | ![](https://gitlab.com/martinshaw/hj-test/-/raw/master/screenshot.jpg?inline=false) | ![](https://gitlab.com/martinshaw/hj-test/-/raw/master/screenshot_mobile.jpg?inline=false) |
