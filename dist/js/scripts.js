var handleTweets, changeTweet;

handleTweets = (tweets) => {
	$("#tweets").html("");
	for (var i = 0; i < tweets.length; i++) {
		$tweet = $(tweets[i]);
		$tweet = $tweet.detach(".interact");
		$tweet = $tweet[0];
		let is_active = (i == 0) ? "active" : "";
		$(".dot").first().addClass(is_active);
		$("#tweets").append($("<li class='tweet "+is_active+"'></li>").html($tweet.innerHTML));
	}
}

changeTweet = (index) => {
	$(".dot").removeClass("active");
	$(".tweet").removeClass("active");
	$(".dot").eq(index).addClass("active");
	$(".tweet").eq(index).addClass("active");
}


$(document).ready(function() {

	var twitterConfig = {
		"profile": {"screenName": 'ukfast'},
		"domId": 'twitter',
		"maxTweets": 4,
		"enableLinks": false, 
		"showUser": false,
		"showTime": false,
		"showImages": false,
		"lang": 'en',
		"showRetweet": false,
		"customCallback": handleTweets,
	};
	twitterFetcher.fetch(twitterConfig);

});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNjcmlwdC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoic2NyaXB0cy5qcyIsInNvdXJjZXNDb250ZW50IjpbInZhciBoYW5kbGVUd2VldHMsIGNoYW5nZVR3ZWV0O1xyXG5cclxuaGFuZGxlVHdlZXRzID0gKHR3ZWV0cykgPT4ge1xyXG5cdCQoXCIjdHdlZXRzXCIpLmh0bWwoXCJcIik7XHJcblx0Zm9yICh2YXIgaSA9IDA7IGkgPCB0d2VldHMubGVuZ3RoOyBpKyspIHtcclxuXHRcdCR0d2VldCA9ICQodHdlZXRzW2ldKTtcclxuXHRcdCR0d2VldCA9ICR0d2VldC5kZXRhY2goXCIuaW50ZXJhY3RcIik7XHJcblx0XHQkdHdlZXQgPSAkdHdlZXRbMF07XHJcblx0XHRsZXQgaXNfYWN0aXZlID0gKGkgPT0gMCkgPyBcImFjdGl2ZVwiIDogXCJcIjtcclxuXHRcdCQoXCIuZG90XCIpLmZpcnN0KCkuYWRkQ2xhc3MoaXNfYWN0aXZlKTtcclxuXHRcdCQoXCIjdHdlZXRzXCIpLmFwcGVuZCgkKFwiPGxpIGNsYXNzPSd0d2VldCBcIitpc19hY3RpdmUrXCInPjwvbGk+XCIpLmh0bWwoJHR3ZWV0LmlubmVySFRNTCkpO1xyXG5cdH1cclxufVxyXG5cclxuY2hhbmdlVHdlZXQgPSAoaW5kZXgpID0+IHtcclxuXHQkKFwiLmRvdFwiKS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcclxuXHQkKFwiLnR3ZWV0XCIpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xyXG5cdCQoXCIuZG90XCIpLmVxKGluZGV4KS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcclxuXHQkKFwiLnR3ZWV0XCIpLmVxKGluZGV4KS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcclxufVxyXG5cclxuXHJcbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xyXG5cclxuXHR2YXIgdHdpdHRlckNvbmZpZyA9IHtcclxuXHRcdFwicHJvZmlsZVwiOiB7XCJzY3JlZW5OYW1lXCI6ICd1a2Zhc3QnfSxcclxuXHRcdFwiZG9tSWRcIjogJ3R3aXR0ZXInLFxyXG5cdFx0XCJtYXhUd2VldHNcIjogNCxcclxuXHRcdFwiZW5hYmxlTGlua3NcIjogZmFsc2UsIFxyXG5cdFx0XCJzaG93VXNlclwiOiBmYWxzZSxcclxuXHRcdFwic2hvd1RpbWVcIjogZmFsc2UsXHJcblx0XHRcInNob3dJbWFnZXNcIjogZmFsc2UsXHJcblx0XHRcImxhbmdcIjogJ2VuJyxcclxuXHRcdFwic2hvd1JldHdlZXRcIjogZmFsc2UsXHJcblx0XHRcImN1c3RvbUNhbGxiYWNrXCI6IGhhbmRsZVR3ZWV0cyxcclxuXHR9O1xyXG5cdHR3aXR0ZXJGZXRjaGVyLmZldGNoKHR3aXR0ZXJDb25maWcpO1xyXG5cclxufSk7Il19
